// users with letter "s" in firstName and d in lastName
db.users.find(
    {$or: [
        {firstName: {$regex: "s", $options: "i"}},
        {lastName: {$regex: "d", $options: "i"}}
        ]
    },
    {
        firstName: 1,
        lastName: 1,
        _id: 0
    }
);

// users from HR department
db.users.find(
    {$and:
        [
            {department: "HR"},
            {age:{$gte: 70}}
        ]
    }
);

// users with letter "e" in firstName and age $lte 30
db.users.find(
    {
        $and: [
            {firstName: {$regex: "e", $options: "i"}},
            {age: {$lte: 30}}
        ]
    }
);